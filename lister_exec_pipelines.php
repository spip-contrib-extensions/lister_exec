<?php

/**
 * Plugin Lister les pages de configurations
 *
 * @plugin     Lister les pages ?exec=xxx
 * @copyright  2013-2021
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\ListerExec\Pipelines
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function lister_exec_listermenu($flux) {
	$flux['data']['lister_exec'] = array(
		'titre' => _T('lister_exec:titre_lister_exec'),
		'icone' => 'prive/themes/spip/images/lister_exec-xx.svg',
	);

	return $flux;
}
